# Polyfab Et Je Code

## Objectifs de la séance 0 

- Introduction au projet Et je code.
- Se familiariser avec le matériel
- Se connecter au Raspberry Pi à partir de son ordinateur
- Visionner l’interface linux du Raspberry Pi apprendre à le naviguer 

## Introduction au Raspberry Pi  

Un Raspberry Pi est un microcontrôleur, c’est-à-dire un circuit intégré rassemblant les éléments essentiels d’un ordinateur (processeur, mémoires et périphériques d’entrées-sorties). Le but premier du Raspberry Pi est la pédagogie. Il permet aux technophiles d’acquérir des compétences en électronique et en programmation en réalisant des projets variés.  

## Votre matériel  

Le microcontrôleur et ses connections 

![Raspberry Pi](images/raspberry_pi.png)

## Configuration réseau  

### Option A : câble Ethernet 

Le Raspberry Pi possède un port Ethernet. Il suffit de connectez votre Raspi à un routeur avec une connexion filaire (câble Ethernet) et l’accès au réseau se fait automatiquement. Il n’y a rien d’autre à configurer !  

### Option B : connexion wifi + utilisation d’un clavier, d’une souris et d’un écran 

Au premier démarrage de Raspbian, vous devriez voir apparaître l’écran appelé « raspi-config ». Si ce n’est pas le cas, pas de panique, vous pouvez y accéder en allant dans le terminal et en tappant : 

`sudo raspi-config`

C’est ici que vous pourrez modifier les paramètres réseaux. En sélectionnant WIRELESS LAN le nom du wifi (SSID) et le mot de passe (passphrase) seront demandés afin de se connecter sur le wifi. Pour naviguer dans le menu de rapsi-config, vous devez utiliser les flèches directionnelles de votre clavier « haut » et « bas ». 

![Configuration réseau](images/network1.png)

L’autre option est de simplement se connecter au wifi comme on le ferait sur son ordinateur portable, en sélectionnant le bon réseau et en entrant le mot de passe : 

![Configuration réseau](images/network2.png)

### Option C : connexion wifi + utilisation d’un ordinateur portable 

Pour vous connecter au wifi lorsque vous utiliser le Raspi avec votre ordinateur portable, vous devez ajouter des informations sur le réseau sur la carde SD de votre Rapsi: 

1. Insérer la carte SD de votre Raspi dans votre ordinateur portable. 

2. Créer un nouveau fichier à la racine de boot appelé : wpa_supplicant.conf. 

    a. Ouvrir Bloc-notes ou un autre logiciel d’édition de texte (à ne pas confondre avec un logiciel de traitement de texte, style Word).  

    b. Coller ensuite ce qui suit dans ce fichier (en tenant compte du nom du réseau et du mot de passe du réseau) : 
<pre><code>
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdevupdate_config=1
    network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
} 
</code></pre>

c. Enregistrer le fichier texte à la racine de boot (s’assurer que le type est All files – on ne veut pas une extension .txt) et le nommer `wpa_supplicant.conf` 
    
Pour du plus d’information, suivre ce [https://desertbot.io/blog/headless-raspberry-pi-3-bplus-ssh-wifi-setup][guide]  étape par étape : 

## Utilisation d’un ordinateur portable pour visualiser l’interface du Raspi 

### Récupérer adresse IP du Raspberry Pi 

Vous aurez besoin de l’adresse IP de votre Raspberry Pi pour vous y connecter à partir de votre ordinateur portable, à l’aide d’une connexion SSH. Cela permet d’utiliser le Raspberry Pi sans écran et clavier.  

Il existe plusieurs moyens de récupérer l’IP du Raspi. Le plus simple est d’exécuter un programme qui permet d’analyser les appareils connectés sur votre réseau et qui vous renvoie les adresses IP afin d’identifier celle de votre Raspi. Nous vous suggérons d’exécuter le logiciel Wireless Network Watcher, disponible ici. Télécharger Wireless Network Watcher en fichier Zip. 

### Connexion en SSH 

La connexion SSH, aussi appelé « Secure Shell » est une technologie réseau qui permet d’accéder à un ordinateur et de pouvoir le manipuler via un terminal et des lignes de commande depuis un autre appareil. Dans notre cas, nous utiliserons la connexion SSH pour accéder au Raspberry Pi (ordinateur) depuis notre ordinateur portable (autre appareil).  

Avant tout, il faut activer les paramètres pour la connexion SSH sur le Rapsberry Pi. Ceux-ci sont désactivés par défaut pour des raisons de sécurité. Il faut donc créer un fichier vide qu’on nomme ssh (sans extension) à la racine de boot sur le carte SD. 

Le reste de la procédure est différente en fonction du système d’exploitation de votre ordinateur.  

#### Option A: Sur Windows 

 Entrer « cmd » dans l’exécuteur de tâche de votre ordinateur portable 

Entrer la commande suivante : 

`ssh pi@votre_adresse_ip`

Vous pouvez alors cliquer sur « Open » et une fenêtre de sécurité « Security Alert » apparaît, cliquez simplement sur « Yes ». Une fenêtre de ligne de commande devrait alors apparaître vous demandant « login as : ». Entrer « pi » puis appuyez sur la touche entrée. Entrer ensuite le mot de passe par défaut « raspberry » et valider avec la touche entrée de votre clavier. 

Utilisateur du logiciel PuTTY 

Si cette première option ne fonctionne pas un logiciel peut être nécessaire pour établir la connexion SSH entre votre Raspberry Pi et Windows. Nous vous proposons d’utiliser le logiciel PuTTY, que vous pouvez télécharger ici. Une fois PuTTY installé, il suffit de lancer le programme et d’inscrire l’adresse IP dans le champ « Host Name » et de valider que le champ « Connection Type » est « SSH ». Une fenêtre de commande apparaîtra ensuite, où il vous sera demandera un nom d’utilisateur et un mot de passe. Nous avons laissé les valeurs par défaut, c’est-à-dire « pi » comme nom d’utilisateur et « raspberry » comme mot de passe. 

#### Option B : Sur Mac ou Linux 

Vous devez simplement ouvrir un terminal et entrer la commande suivante : 

`ssh pi@votre_adresse_ip`

Une fenêtre de ligne de commande devrait alors apparaître vous demandant « login as : ». Entrer « pi » puis appuyez sur la touche entrée. Entrer ensuite le mot de passe par défaut « raspberry » et valider avec la touche entrée de votre clavier. 

## En savoir plus  

Avant de vous remettre vos kits, nous avons fait l’installation de Raspian sur votre Raspberry Pi. Comme mentionné dans la liste de matériel, votre kit contient une carte SD Noobs. Avant d’aller plus loin, clarifions quelques notions : 

### C’est quoi Noobs ? 

Noobs est un utilitaire qui simplifie grandement l’installation d’un système d’exploitation sur votre Raspeberry Pi. C’est un gestionnaire d’installation spécialement conçu pour Raspberry Pi et il vous permettra de choisir parmi une grande liste d’OS et de l’installer très rapidement. 

Les kits de démarrage Raspeberry Pi contiennent souvent une carte SD formatée avec Noobs. Si vous n’avez qu’une carte SD vierge, vous devez installer Noobs dessus. Cliquer ici pour comment installer Noobs sur une carte SD vierge. 

###  C’est quoi Raspbian ? 

Raspbian est le système d’exploitation de référence pour Raspberry Pi. Il est basé sur Linux Debian. Bien que Raspbian soit le système d’exploitation le plus populaire, il existe d’autres alternatives qui sont listées ici. 