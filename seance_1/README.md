# Polyfab Et Je Code

## Objectifs de la séance 1

- Se familiariser avec le matériel (broches du raspi, fonctionnement d’un breadboard, LED & résistance)
- Programmer un premier programme en Python sur le raspi (allumer/éteindre une LED --> output + bouton --> input)
- Récupération d’entrées et envoyer des sortie (broches GPIO) 

## Résumé de la séance  
1. Réaliser un circuit pour allumer et éteindre une LED en faisant les branchements nécessaires avec les composantes (LED, résistance, broches du Raspi)
1. Écrire le code afin de contrôler automatiquement la LED
1. Allumer la LED (faire rouler le code) 

## Le matériel 

### Breadboard 

![Raspberry Pi](images/breadboard.png)
Le « breadboard » est un moyen de connecter des composants électroniques entre eux sans avoir à les souder. Elles sont souvent utilisées pour tester la conception d’un circuit avant de créer une carte de circuit imprimé (PCB). 

### LED et résistance 

Une LED a une jambe plus longue que l’autre. La jambe la plus longue (appelée « anode ») est toujours connectée à l’alimentation positive du circuit. La jambe la plus courte (appelée « cathode ») est connectée au côté négatif de l’alimentation électrique, appelé « masse ». LED signifie Light Emitting Diode (diode électroluminescente) et s’allume lorsque l’électricité passe à travers elle. Les LED ne fonctionnent que si l’alimentation est fournie dans le bon sens (c’est à-dire si la « polarité » est correcte). Vous ne risquez pas de casser les LED si vous les branchez dans le mauvais sens, elles ne s’allumeront tout simplement pas. 

Vous devez TOUJOURS utiliser des résistances pour connecter les LED aux broches GPIO du Raspi. Le Raspi ne peut fournir qu’un faible courant (environ 60mA). Les LEDs voudront en tirer plus, et si on les laisse faire, elles grilleront votre Raspi. Par conséquent, en plaçant les résistances dans le circuit, on s’assurera que seul ce petit courant circulera et le Raspi ne sera pas endommagé. Les résistances sont un moyen de limiter la quantité d’électricité qui passe dans un circuit ; plus précisément, elles limitent la quantité de « courant » qui peut circuler. La mesure de la résistance s’appelle l’Ohm (Ω), et plus la résistance est grande, plus elle limite le courant. La valeur d’une résistance est indiquée par des bandes colorées sur toute la longueur du corps de la résistance. 

### Votre Raspi 

Votre Raspi est actuellement contenu dans son un boitier qui permet de le protéger. Ouvrez-le! 

Vous remarquerez deux rangées de « broches ». Ces broches sont appelées broches GPIO (general-purpose input/output). Ce sont ses broches qui permettent à votre Raspi de contrôler des LED, de les allumer ou de les éteindre, ou des moteurs, ou bien d’autres choses encore. Il est également capable de détecter si un interrupteur a été actionné, ou la température, ou la lumière. 

Ce guide interactif vous permet de vous familiariser avec les fonctionnalités particulières de chacune des broches de votre raspi : [Guide complet](https://fr.pinout.xyz/#)


### Le circuit 

![Circuit 1](images/circuit_1.png)

### Le code 

mporter la library python – `sudo apt-get install python-rpi.gpio python3-rpi.gpio` #Install le module qui permet de contrôler les pins GPIO du raspberry Pi à partir du language python 

Créer un nouveau fichier pour y écrire le code (les commandes) qui permettra d’allumer la led   `nano led.py` 

Écrire le code suivant : 
[Code](led.py)

Dans le terminal pour exécuter le code `python3 led.py`

## Références
[Circuit](https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins)

[Code](https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins)