import RPi.GPIO as GPIO  # Importer la librairie RPi.GPIO

import time  # Importer la librairie time

GPIO.setmode(GPIO.BCM)  # BCM  = GPIO
GPIO.setwarnings(False)
GPIO.setup(18, GPIO.OUT)  # Configuration de la broche 18 en sortie


GPIO.output(18, GPIO.HIGH)  # Allumer la broche 18 - on envoi du courant
print("LED on")

time.sleep(1)  # Pause de 1 sec


GPIO.output(18, GPIO.LOW)  # Eteindre la broche
print("LED off")